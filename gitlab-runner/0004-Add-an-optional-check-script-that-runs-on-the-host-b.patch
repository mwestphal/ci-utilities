From 46b0ab691f09eea06daa0cff9fe3b5ae88ba4377 Mon Sep 17 00:00:00 2001
From: Brad King <brad.king@kitware.com>
Date: Wed, 29 Jul 2020 15:38:29 -0400
Subject: [PATCH 4/5] Add an optional check script that runs on the host before
 the executor

Give local runner configurations a chance to check the job before
running it.
---
 commands/multi.go | 25 +++++++++++++++++++++++++
 common/config.go  |  1 +
 2 files changed, 26 insertions(+)

diff --git a/commands/multi.go b/commands/multi.go
index 964935d3f..9d8c11af0 100644
--- a/commands/multi.go
+++ b/commands/multi.go
@@ -8,6 +8,7 @@ import (
 	"net/http"
 	"net/http/pprof"
 	"os"
+	"os/exec"
 	"os/signal"
 	"runtime"
 	"sync"
@@ -679,6 +680,30 @@ func (mr *RunCommand) processBuildOnRunner(
 	// to speed up taking the builds
 	mr.requeueRunner(runner, runners)
 
+	if runner.HostCheckScript != "" {
+		var hostCheckCmd *exec.Cmd
+		if runtime.GOOS == "windows" {
+			hostCheckCmd = exec.Command("cmd", "/c", runner.HostCheckScript)
+		} else {
+			hostCheckCmd = exec.Command("sh", "-c", runner.HostCheckScript)
+		}
+		if hostCheckCmd == nil {
+			hostCheckCmdErr := errors.New("failed to construct host check script command")
+			trace.Fail(hostCheckCmdErr, common.JobFailureData{Reason: common.RunnerSystemFailure})
+			return hostCheckCmdErr
+		}
+		hostCheckCmd.Env = append(append(os.Environ(), runner.Environment...),
+		                          build.JobResponse.Variables.StringList()...)
+		hostCheckOut, hostCheckErr := hostCheckCmd.CombinedOutput()
+		if hostCheckErr != nil {
+			var logger = common.NewBuildLogger(trace, build.Log())
+			logger.Println(string(hostCheckOut))
+			trace.Fail(errors.New("host check script failed"),
+			           common.JobFailureData{Reason: common.ScriptFailure, ExitCode: 1})
+			return hostCheckErr
+		}
+	}
+
 	// Process a build
 	return build.Run(mr.getConfig(), trace)
 }
diff --git a/common/config.go b/common/config.go
index f9c721b2e..bb2b73774 100644
--- a/common/config.go
+++ b/common/config.go
@@ -916,6 +916,7 @@ type RunnerSettings struct {
 	CloneURL  string `toml:"clone_url,omitempty" json:"clone_url" long:"clone-url" env:"CLONE_URL" description:"Overwrite the default URL used to clone or fetch the git ref"`
 
 	Environment     []string `toml:"environment,omitempty" json:"environment" long:"env" env:"RUNNER_ENV" description:"Custom environment variables injected to build environment"`
+	HostCheckScript string   `toml:"host_check_script,omitempty" description:"Runner-specific command script executed on host before running job"`
 	PreCloneScript  string   `toml:"pre_clone_script,omitempty" json:"pre_clone_script" long:"pre-clone-script" env:"RUNNER_PRE_CLONE_SCRIPT" description:"Runner-specific command script executed before code is pulled"`
 	PostCloneScript string   `toml:"post_clone_script,omitempty" json:"post_clone_script" long:"post-clone-script" env:"RUNNER_POST_CLONE_SCRIPT" description:"Runner-specific command script executed just after code is pulled"`
 	PreBuildScript  string   `toml:"pre_build_script,omitempty" json:"pre_build_script" long:"pre-build-script" env:"RUNNER_PRE_BUILD_SCRIPT" description:"Runner-specific command script executed just before build executes"`
-- 
2.38.1

