#!/bin/sh

set -e

die () {
    echo >&2 "$@"
    exit 1
}

processor="$( uname -m )"
readonly processor
case "$processor" in
    x86_64)
        ;;
    *)
        die "Unsupported processor: $processor"
        ;;
esac

os="$( uname -s )"
readonly os
case "$os" in
    Linux)
        flags_line="$( grep '^flags' /proc/cpuinfo | sort -u )"
        platform="linux"
        ;;
    *)
        die "Unknown OS: $os"
        ;;
esac
readonly flags_line
readonly platform

flags="$( echo "$flags_line" | cut -d: -f2- )"
readonly flags

readonly dir="${0%/*}"
echo "$flags" | "$dir/abi-level-$platform.awk"
