#!/bin/bash

set -e

# Cross compilation is not supported by this script.
readonly target_arch="$( uname -m )"

case "$target_arch" in
    x86_64)
        min_macosx_version="10.13"
        arch="x86_64"
        target="$arch-apple-darwin"
        ;;
    arm64)
        min_macosx_version="11.0"
        arch="aarch64"
        target="$arch-apple-darwin"
        ;;
    *)
        echo >&2 "Unknown architecture: $target_arch"
        exit 1
        ;;
esac
readonly min_macosx_version
readonly arch
readonly target

readonly gmp_version="6.2.1"
readonly gmp_filename="gmp-$gmp_version.tar.xz"
readonly gmp_url="https://ftp.gnu.org/gnu/gmp/$gmp_filename"
readonly gmp_sha256sum="fd4829912cddd12f84181c3451cc752be224643e87fac497b69edddadc49b4f2"

readonly mpfr_version="4.1.0"
readonly mpfr_filename="mpfr-$mpfr_version.tar.xz"
readonly mpfr_url="https://www.mpfr.org/mpfr-$mpfr_version/$mpfr_filename"
readonly mpfr_sha256sum="0c98a3f1732ff6ca4ea690552079da9c597872d30e96ec28414ee23c95558a7f"

readonly mpc_version="1.2.1"
readonly mpc_filename="mpc-$mpc_version.tar.gz"
readonly mpc_url="https://ftp.gnu.org/gnu/mpc/$mpc_filename"
readonly mpc_sha256sum="17503d2c395dfcf106b622dc142683c1199431d095367c6aacba6eec30340459"

readonly gcc_version="12.2"
readonly gcc_filename="gcc-$gcc_version-darwin-r0.tar.gz"
readonly gcc_url="https://github.com/iains/gcc-12-branch/archive/refs/tags/$gcc_filename"
readonly gcc_sha256sum="3cef48e3b706ccbf37d2739bbe50f3420e99d707eee7608f9c86c94338f4bc07"

. gfortran/macos-utils.sh

mkdir build
cd build

download_verify_extract gmp "$gmp_url" "$gmp_sha256sum" "$gmp_filename"
download_verify_extract mpfr "$mpfr_url" "$mpfr_sha256sum" "$mpfr_filename"
download_verify_extract mpc "$mpc_url" "$mpc_sha256sum" "$mpc_filename"
download_verify_extract gcc "$gcc_url" "$gcc_sha256sum" "$gcc_filename"

export CC=clang
export CPP=/usr/bin/cpp
export CXX=clang++

prefix="/opt/gcc"
destination="$( pwd )/gcc-$gcc_version-$arch"

export LDFLAGS="-Wl,-headerpad_max_install_names -Wl,-rpath,@loader_path -Wl,-rpath,$destination/$prefix/lib"
export CFLAGS="-mmacosx-version-min=$min_macosx_version --sysroot=$DEVELOPER_DIR/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk"
export CXXFLAGS="-mmacosx-version-min=$min_macosx_version --sysroot=$DEVELOPER_DIR/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk"

echo "Building for $arch-apple-darwin"

build_dep gmp "$prefix" "$destination" \
    --build "$target"
build_dep mpfr "$prefix" "$destination" \
    --build "$target" \
    --with-gmp="$destination/$prefix"
build_dep mpc "$prefix" "$destination" \
    --build "$target" \
    --with-{gmp,mpfr}="$destination/$prefix"

# `fcntl.h` detection fails because `defined(__has_feature)` doesn't
# have an open paren after it. Fake it.
export ac_cv_header_fcntl_h=yes

build gcc "$prefix" "$destination" \
    --with-{gmp,mpfr,mpc}="$destination/$prefix" \
    --with-sysroot="$DEVELOPER_DIR/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk" \
    --with-stage1-ldflags="-Wl,-rpath,$destination/$prefix/lib -Wl,-headerpad_max_install_names" \
    --with-boot-ldflags="-Wl,-rpath,$destination/$prefix/lib -Wl,-rpath,@executable_path/../lib -Wl,-rpath,@executable_path/../../../../lib -Wl,-headerpad_max_install_names -static-libstdc++ -static-libgcc"

cd ..

tar -C build -cJf "gcc-$gcc_version-macos$min_macosx_version-$arch.tar.xz" "gcc-$gcc_version-$arch"
