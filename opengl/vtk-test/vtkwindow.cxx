#include <vtkActor.h>
#include <vtkCamera.h>
#include <vtkCellArray.h>
#include <vtkNew.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>

#include <cstdlib>

int main(int argc, char* argv[]) {
    std::array<std::array<double, 3>, 8> pts = { { { { 0, 0, 0 } }, { { 1, 0, 0 } }, { { 1, 1, 0 } },
    { { 0, 1, 0 } }, { { 0, 0, 1 } }, { { 1, 0, 1 } }, { { 1, 1, 1 } }, { { 0, 1, 1 } } } };
    std::array<std::array<vtkIdType, 4>, 6> ordering = { { { { 0, 1, 2, 3 } }, { { 4, 5, 6, 7 } },
    { { 0, 1, 5, 4 } }, { { 1, 2, 6, 5 } }, { { 2, 3, 7, 6 } }, { { 3, 0, 4, 7 } } } };

    vtkIdType i = 0;
    vtkNew<vtkPoints> points;
    for (auto const& pt : pts) {
        points->InsertPoint(i++, pt.data());
    }

    vtkNew<vtkCellArray> polys;
    for (auto& order : ordering) {
        polys->InsertNextCell(order.size(), order.data());
    }

    vtkNew<vtkPolyData> cube;
    cube->SetPoints(points);
    cube->SetPolys(polys);

    vtkNew<vtkPolyDataMapper> mapper;
    mapper->SetInputData(cube);

    vtkNew<vtkActor> actor;
    actor->SetMapper(mapper);

    vtkNew<vtkCamera> cam;
    cam->SetPosition(1, 1, 1);
    cam->SetFocalPoint(0, 0, 0);

    vtkNew<vtkRenderer> ren;
    vtkNew<vtkRenderWindow> win;
    win->AddRenderer(ren);

    vtkNew<vtkRenderWindowInteractor> iren;
    iren->SetRenderWindow(win);

    ren->AddActor(actor);
    ren->SetActiveCamera(cam);
    ren->ResetCamera();

    win->SetSize(600, 600);

    win->Render();
    iren->Start();

    return EXIT_SUCCESS;
}
