# Install toolchain dependencies.
apt-get update
apt-get install -y \
  gcc-mingw-w64-x86-64

# Add support for needed architectures.
rustup target add x86_64-pc-windows-gnu

# Report result for human reference.
rustc --version
