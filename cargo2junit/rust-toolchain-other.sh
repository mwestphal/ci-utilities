# Install toolchain dependencies.
apt-get update

# Add support for needed architectures.
rustup target add x86_64-unknown-linux-gnu

# Report result for human reference.
rustc --version
