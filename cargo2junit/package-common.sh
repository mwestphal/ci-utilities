readonly git_url='https://github.com/johnterickson/cargo2junit.git'
readonly git_commit='01041853b9e1dd47395a007f109733cdc635e960' # master
readonly version='master'

git clone "$git_url" cargo2junit/src
cd cargo2junit/src
git -c advice.detachedHead=false checkout "$git_commit"
